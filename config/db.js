const mongoose = require('mongoose')

mongoose.connect(process.env.DB_HOST)

const db = mongoose.connection

db.on('error', () => console.error('Cannot connect to the database'))
db.once('open', () => console.log('Successfully connected to the database'))

module.exports = db