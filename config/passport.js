const jwt = require('jsonwebtoken')
const passport = require('passport')
const passportJWT = require('passport-jwt')
const ExtractJwt = passportJWT.ExtractJwt
const JwtStrategy = passportJWT.Strategy

const User = require('models/User')

const jwtOptions = {}
jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeader()
jwtOptions.secretOrKey = process.env.SECRET_KEY

const strategy = new JwtStrategy(jwtOptions, (jwtPayload, next) => {
  console.log('jwtPayload', jwtPayload)
  User.findById(jwtPayload.id)
    .then(user => next(null, user), err => next(err, null))
})

passport.use(strategy)

module.exports = passport