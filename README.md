# GoldSaver

**Github**: [https://github.com/CNPMteam7/GoldSaver-API](https://github.com/CNPMteam7/GoldSaver-API)  
**Heroku**: [https://goldsaver.herokuapp.com/](https://goldsaver.herokuapp.com/)  
**API**: [https://app.swaggerhub.com/apis/pexea12/GoldSaver/1.0.0](https://app.swaggerhub.com/apis/pexea12/GoldSaver/1.0.0)

## Manual Configuration

For local databse use, add folder `database` and run
```bash
$ mongod --dbpath=database --port=3001
```  

Add a `.env` file which includes: `DB_HOST`, `PORT` and `SECRET_KEY`    

For example:  
```
DB_HOST=mongodb://localhost:3001/goldsaver
PORT=3000
SECRET_KEY=lac_troi
```

## Development
```
yarn && yarn dev
```

## Sprint backlog
[Sprint backlog](https://docs.google.com/spreadsheets/d/1UXG9eh9g8ukKhWNHSEsBVj0byFqYf61Y9mgijbquj3M/edit?ts=581df650#gid=0)

## Product backlog
[Product backlog](https://docs.google.com/spreadsheets/d/1wdg8mBkRizpu4jXyxo85lT0eclGUyTa0-9UW2q0ePwo/edit#gid=0)
