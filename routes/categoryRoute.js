const express = require('express') 
const _ = require('lodash')

const router = express.Router()

const Category = require('models/Category')
const Activity = require('models/Activity')

const memberAccess = require('utils/memberAccess')

module.exports = () => {
  router.get('/', memberAccess, (req, res) => {
    const currentUser = req.user

    const options = {
      limit: req.query.limit || 10,
      skip: req.query.skip || 0,
    }

    Category.find({ user_id: currentUser._id }) 
      .skip(options.skip)
      .limit(options.limit)
      .then(categories => res.json(categories), err => res.status(500).send(err))
  })

  router.get('/all', memberAccess, (req, res) => {
    const currentUser = req.user

    if (currentUser.role !== 'admin')
      return res.status(403).json({ message: 'Admin access required' })

    const options = {
      limit: req.query.limit || 10,
      skip: req.query.skip || 0,
    }

    Category.find({})
      .skip(options.skip)
      .limit(options.limit)
      .then(categories => res.json(categories), err => res.status(500).send(err))
  })

  router.post('/', memberAccess, (req, res) => {
    const currentUser = req.user

    const body = _.pick(req.body, [
      'name', 
      'icon',
    ])

    body.user_id = currentUser._id

    const newCategory = new Category(body)

    newCategory.save()
      .then((category) => {
        category = _.omit(category.toObject(), [ '__v' ])
        res.json(category)
      }, err => res.status(400).send(err))
  })

  router.get('/:category_id', memberAccess, (req, res) => {
    const currentUser = req.user

    Category.findById(req.params.category_id)
      .then((category) => {
        if (!category) return res.status(404).json({ message: 'Category not found'})
        if (!category.user_id.equals(currentUser._id))
          return res.status(403).json({ message: 'Unable to see this category' })

        Activity.find({ category_id: category._id })
          .select('-user_id -category_id')
          .then((activities) => {1
            category.activities = activities
            res.json(category)
          }, err => res.status(500).send())

      }, err => res.status(404).send(err))
  })

  router.delete('/:category_id', memberAccess, function(req, res, next) {
    const currentUser = req.user

    Category.findById(req.params.category_id)
      .then((category) => {
        if (!category.user_id.equals(currentUser._id))
          return res.status(403).json({ message: 'Unable to delete this category' })

        category.remove()
          .then(() => res.json({ message: 'Category removed' }))
      })
  })

  router.patch('/:category_id', memberAccess, function(req, res, next) {
    const currentUser = req.user

    const body = _.pick(req.body, [
      'name',
      'icon',
    ])

    Category.findById(req.params.category_id) 
      .then((category) => {
        if (!category.user_id.equals(currentUser._id))
          return res.status(403).json({ message: 'Unable to change this category' })

        _.forOwn(body, (value, key) => category[key] = value)
        category.save()
          .then((category) => {
            category = _.omit(category.toObject(), [ '__v' ])
            res.json(category)
          }, err => res.status(400).send(err))
      })
  })

  router.get('/:category_id/activities', memberAccess, (req, res) => {
    const currentUser = req.user

    Category.findById(req.params.category_id)
      .then((category) => {
        if (!category.user_id.equals(currentUser._id))
          return res.status(403).json({ message: 'Unable to access this category.' })

        Activity.find({ category_id: req.params.category_id })
          .then((activities) => res.json(activities), err => res.status(500).send(err))
      }, err => res.status(404).json(err))
  })
    
  return router
}