const express = require('express')
const _ = require('lodash')
const jwt = require('jsonwebtoken')

const router = express.Router()

const User = require('models/User')
const Wallet = require('models/Wallet')
const Category = require('models/Category')
const memberAccess = require('utils/memberAccess')

const defaultCategories = require('models/defaultCategories')

module.exports = () => {
  router.post('/register', (req, res) => {
    const body = _.pick(req.body, [
      'email',
      'password',
      'confirm_password',
      'name',
    ])

    if (body.password != body.confirm_password)
      return res.status(400).json({ message: 'Password does not match' })

    const newUser = new User(body)

    newUser.save()
      .then((user) => {
        user = _.omit(user.toObject(), [ '__v', 'password' ])
        const token = 'JWT ' + jwt.sign({ id: user._id }, process.env.SECRET_KEY)

        defaultCategories.forEach((category) => {
          category = new Category(category)
          category.user_id = user._id
          category.save()
        })

        return res.json({ user, token })
      }, err => res.status(400).json(err))
  })

  router.post('/login', (req, res) => {
    const body = _.pick(req.body, [
      'email',
      'password',
    ])

    if (!body.password || !body.email)
      return res.status(400).json({ message: 'Email and password required' })

    User.findOne({ email: body.email })
      .select('+password')
      .then((user) => {
        if (!user)
          return res.status(404).json({ message: 'Wrong email' })

        user.comparePassword(body.password)
          .then((isMatched) => {
            if (!isMatched)
              return res.status(401).json({ message: 'Wrong password' })

            user = _.omit(user.toObject(), [ '__v', 'password' ])
            const token = 'Bearer ' + jwt.sign({ id: user._id }, process.env.SECRET_KEY)
            res.json({ user, token }) 
          })
        })
  })

  router.get('/me', memberAccess, (req, res) => {
    res.json(req.user)
  })

  router.patch('/me', memberAccess, (req, res) => {
    const body = _.pick(req.body, [
      'email',
      'password',
      'confirm_password',
      'name',
      'avatar',
    ])

    if (body.password || body.confirm_password) 
      if (body.password !== body.confirm_password)
        return res.status(400).json({ message: 'Password does not match' })

    const currentUser = req.user
    _.forOwn(body, (value, key) => currentUser[key] = value)

    currentUser.save()
      .then((user) => {
        user = _.omit(user.toObject(), [ 'password' ])
        res.json(user)
      }, err => res.status(400).send(err))
  })

  router.get('/', memberAccess, (req, res) => {
    const currentUser = req.user

    if (currentUser.role !== 'admin')
      return res.status(403).json({ message: 'Admin access required' })

    const body = _.pick(req.query, [
      'skip',
      'limit',
      'id',
      'name'
    ])

    const options = {
      limit: body.limit || 10,
      skip: body.skip || 0,
    }

    const filter = []
    if (body.id) filter.push({ _id: body.id })
    if (body.name) filter.push({ name: new RegExp(body.name, 'i') })

    const query = filter.length === 0 ? {} : { $and: filter }

    User.find(query)
      .skip(options.offset)
      .limit(options.limit)
      .then(users => res.json(users), err => res.status(404).send(err))
  })

  router.delete('/:user_id', memberAccess, (req, res) => {
    const currentUser = req.user

    if (currentUser.role !== 'admin')
      return res.status(403).json({ message: 'Admin access required' })

    User.findByIdAndRemove(req.params.user_id)
      .then((user) => {
        if (!user) return res.status(404).json({ message: 'User not found' })
        res.json({ message: 'Successfully removed' })
      }, err => res.status(400).send(err))
  })

  router.patch('/:user_id', memberAccess, (req, res) => {
    const currentUser = req.user

    if (currentUser.role !== 'admin')
      return res.status(403).json({ message: 'Admin access required' })

    const body = _.pick(req.body, [
      'email',
      'password',
      'name',
      'avatar',
      'role',
    ])

    User.findById(req.params.user_id)
      .then((user) => {
        if (!user) res.status(404).json({ message: 'User not found' })
        _.forOwn(body, (value, key) => user[key] = value)
        user.save()
          .then((user) => {
            user = _.omit(user.toObject(), [ 'password' ])
            res.json(user)
          }, err => res.status(400).send(err))
      }, err => res.status(400).send(err))
  })

  router.get('/:user_id', memberAccess, (req, res) => {
    const currentUser = req.user

    if (currentUser.role !== 'admin')
      return res.status(403).json({ message: 'Admin access required' })

    User.findById(req.params.user_id)
      .then((user) => {
        if (!user) res.status(404).json({ message: 'User not found' })
        Wallet.find({ user_id: req.params.user_id }) 
        .then((wallets) => {
          user.wallets = wallets
          res.json(user)
        }, err => res.status(404).send(err))
      }, err => res.status(404).send(err))
  })

  return router
}
