const express = require('express')
const _ = require('lodash')

const router = express.Router()

const User = require('models/User')
const Wallet = require('models/Wallet')
const Activity = require('models/Activity')

const memberAccess = require('utils/memberAccess')

module.exports = () => {
	router.get('/', memberAccess, (req, res) => {
		const currentUser = req.user

    const options = {
      limit: req.query.limit || 10,
      skip: req.query.skip || 0,
    }

    Activity.find({ user_id: currentUser._id }) 
    	.skip(options.skip)
    	.limit(options.limit)
      .then(activities => res.json(activities), err => res.status(500).send(err))
	})

	router.get('/all', memberAccess, (req, res) => {
		const currentUser = req.user

		if (currentUser.role !== 'admin')
			return res.status(403).json({ message: 'Admin access required' })

		const options = {
      limit: req.query.limit || 10,
      skip: req.query.skip || 0,
    }

    Activity.find({})
    	.skip(options.skip)
    	.limit(options.limit)
    	.then(activities => res.json(activities), err => res.status(500).send(err))
	})

	router.get('/:activity_id', memberAccess, (req, res) => {
		const currentUser = req.user

		Activity.findById(req.params.activity_id)
			.then((activity) => {
				if (!activity.user_id.equals(currentUser._id))
					return res.status(403).json({ message: 'Unable to access this activity' })

				if (!activity) return res.status(404).json({ message: 'Activity not found' })
				res.json(activity)
			}, err => res.status(500).send(err))
	})

	router.patch('/:activity_id', memberAccess, (req, res) => {
		const currentUser = req.user

		Activity.findById(req.params.activity_id)
			.then((activity) => {
				if (!activity) 
					return res.status(404).json({ message: 'Activity not found' })

				if (!activity.user_id.equals(currentUser._id))
					return res.status(403).json({ message: 'Unable to access this activity' })

				const body = _.pick(req.body, [
					'category',
					'note',
					'cost',
					'date',
				])

				Wallet.findById(activity.wallet_id)
					.then((wallet) => {
						if (activity.cost > 0) wallet.outflow -= activity.cost 
						else wallet.inflow -= -activity.cost 

						_.forOwn(body, (value, key) => activity[key] = value)

						if (activity.cost > 0) wallet.outflow += activity.cost 
						else wallet.inflow += -activity.cost 

						if (wallet.total < 0)
							return res.status(400).json({ message: 'Unable to change this activity. Total < 0' })

						wallet.save()

						activity.save()
						.then((activity) => {
							activity = _.omit(activity.toObject(), [ '__v' ])
							res.json(activity)
						}, err => res.status(500).send(err))

					}, err => res.status(500).send(err))
		
			}, err => res.status(500).send(err))
	})

	router.delete('/:activity_id', memberAccess, (req, res) => {
		const currentUser = req.user

		Activity.findById(req.params.activity_id)
			.then((activity) => {
				if (!activity) 
					return res.status(404).json({ message: 'Activity not found' })

				if (!activity.user_id.equals(currentUser._id))
					return res.status(403).json({ message: 'Unable to access this activity' })

				Wallet.findById(activity.wallet_id)
					.then((wallet) => {
						if (!wallet) 
							return res.status(404).json({ message: 'Wallet of activity not found' })

						if (activity.cost > 0)
							wallet.outflow -= activity.cost
						else wallet.inflow -= activity.cost

						if (activity.total < 0)
							return res.status(400).json({ message: 'Unable to remove activity. Total < 0'})

						wallet.save()
						activity.remove()
							.then(() => res.json({ message: 'Remove successfully' }), err => res.status(500).send(err))
					}, err => res.status(500).send(err))
			}, err => res.status(500).send(err))
	})

	router.patch('/:activity_id/categories/:category_id', (req, res) => {
		const findActivity = Activity.findById(req.params.activity_id)
			.then((activity) => {
				if (!activity) 
					return res.status(404).json({ message: 'Activity not found' })

				if (!activity.user_id.equals(currentUser._id))
					return res.status(403).json({ message: 'Unable to access this activity' })

				return activity
			}, err => res.status(500).send(err))

		const findCategory = Category.findById(req.params.category_id)
			.then((category) => {
				if (!category)
					return res.status(404).json({ message: 'Category not found'})

				if (!category.user_id.equals(currentUser._id))
					return res.status(403).json({ message: 'Unable to access this category' })

				return category
			}, err => res.status(500).send(err))

		Promise.all([
				findActivity,
				findCategory,
			])
			.then((res) => {
				activity = res[0]
				category = res[1]

				activity.category_id = category._id

				activity.save()
					.then((category) => res.status(200).json({ message: 'Attach category successfully' }), err => res.status(500).send(err))
			})
	})

	return router
}