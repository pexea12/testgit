const express = require('express') 
const _ = require('lodash')

const router = express.Router()

const User = require('models/User')
const Wallet = require('models/Wallet')
const Activity = require('models/Activity')

const memberAccess = require('utils/memberAccess')


module.exports = () => {
  router.get('/', memberAccess, (req, res) => {
    const currentUser = req.user

    const options = {
      limit: req.query.limit || 10,
      skip: req.query.skip || 0,
    }

    Wallet.find({ user_id: currentUser._id }) 
      .skip(options.skip)
      .limit(options.limit)
      .then(wallets => res.json(wallets), err => res.status(500).send(err))
  })

  router.get('/all', memberAccess, (req, res) => {
    const currentUser = req.user

    if (currentUser.role !== 'admin')
      return res.status(403).json({ message: 'Admin access required' })

    const options = {
      limit: req.query.limit || 10,
      skip: req.query.skip || 0,
    }

    Wallet.find({})
      .skip(options.skip)
      .limit(options.limit)
      .then(wallets => res.json(wallets), err => res.status(500).send(err))
  })

  router.post('/', memberAccess, (req, res) => {
    const currentUser = req.user

    const body = _.pick(req.body, [
      'name', 
      'currency',
      'inflow',
      'outflow',
      'icon',
    ])

    body.user_id = currentUser._id

    const newWallet = new Wallet(body)

    newWallet.save()
      .then((wallet) => {
        wallet = _.omit(wallet.toObject(), [ '__v' ])
        res.json(wallet)
      }, err => res.status(400).send(err))
  })

  router.get('/:wallet_id', memberAccess, (req, res) => {
    const currentUser = req.user

    Wallet.findById(req.params.wallet_id)
      .then((wallet) => {
        if (!wallet) return res.status(404).json({ message: 'Wallet not found'})
        if (!wallet.user_id.equals(currentUser._id))
          return res.status(403).json({ message: 'Unable to see this wallet' })

        Activity.find({ wallet_id: wallet._id })
          .select('-user_id -wallet_id')
          .then((activities) => {
            wallet.activities = activities
            res.json(wallet)
          }, err => res.status(500).send())

      }, err => res.status(404).send(err))
  })

  router.delete('/:wallet_id', memberAccess, function(req, res, next) {
    const currentUser = req.user

    Wallet.findById(req.params.wallet_id)
      .then((wallet) => {
        if (!wallet.user_id.equals(currentUser._id))
          return res.status(403).json({ message: 'Unable to delete this wallet' })

        wallet.remove()
          .then(() => res.json({ message: 'Wallet removed' }))
      })
  })

  router.patch('/:wallet_id', memberAccess, function(req, res, next) {
    const currentUser = req.user

    const body = _.pick(req.body, [
      'name',
      'currency',
      'inflow',
      'outflow',
      'icon',
    ])

    Wallet.findById(req.params.wallet_id) 
      .then((wallet) => {
        if (!wallet.user_id.equals(currentUser._id))
          return res.status(403).json({ message: 'Unable to change this wallet' })

        _.forOwn(body, (value, key) => wallet[key] = value)
        wallet.save()
          .then((wallet) => {
            wallet = _.omit(wallet.toObject(), [ '__v' ])
            res.json(wallet)
          }, err => res.status(400).send(err))
      })
  })

  router.post('/:wallet_id/activities', memberAccess, (req, res) => {
    const currentUser = req.user

    const body = _.pick(req.body, [
      'cost',
      'date',
      'note',
    ])

    const newActivity = new Activity(body)
    newActivity.user_id = currentUser._id

    Wallet.findById(req.params.wallet_id)
      .then((wallet) => {
        if (!wallet) return res.status(404).json({ message: 'Wallet not found' })
          
        if (activity.cost > 0)
          wallet.outflow += activity.cost
        else wallet.inflow += -activity.cost

        if (wallet.total < 0)
          return res.status(400).json({ message: 'Unable to add this activity. Total < 0' })

        wallet.save()

        newActivity.wallet_id = wallet._id
        newActivity.save()
          .then((activity) => {
            activity = _.omit(activity.toObject(), [ '__v' ])
            res.json(activity)
          }, err => res.status(400).send(err))

      }, err => res.status(500).send(err))
    
  })

  router.get('/:wallet_id/activities', memberAccess, (req, res) => {
    const currentUser = req.user

    Wallet.findById(req.params.wallet_id)
      .then((wallet) => {
        if (!wallet.user_id.equals(currentUser._id))
          return res.status(403).json({ message: 'Unable to access this wallet.' })

        Activity.find({ wallet_id: req.params.wallet_id })
          .then((activities) => res.json(activities), err => res.status(500).send(err))
      }, err => res.status(404).json(err))
  })
    
  return router
}