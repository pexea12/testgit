const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const cors = require('cors')
mongoose.Promise = global.Promise

require('dotenv').config()

const env = process.env.NODE_ENV || 'development'
const port = process.env.PORT || 3000

const app = express()

const db = require('config/db')
const passport = require('config/passport')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(passport.initialize())
app.use(cors())

const userRoute = require('routes/userRoute')()
const walletRoute = require('routes/walletRoute')()
const activityRoute = require('routes/activityRoute')()
const categoryRoute = require('routes/categoryRoute')()

app.get('/api', (req, res) => {
  res.end('Welcome to GoldSaver API')
})

app.use('/api/users', userRoute)
app.use('/api/wallets', walletRoute)
app.use('/api/activities', activityRoute)
app.use('/api/categories', categoryRoute)

app.use(express.static('build'))
app.use('/public', express.static('public'))


app.listen(port, () => {
  console.log(`Listening on port ${ port }`)
})
