const url = process.env.PUBLIC_URL || ''

module.exports = [
  {
    name: 'Lover',
    icon: `${ url }/images/icon/sweetheart.png`
  },
  {
    name: 'Food and drink',
    icon: `${ url }/images/icon/food-and-drink.png`
  },
  {
    name: 'Cafe',
    icon: `${ url }/images/icon/coffee-pot.png`
  },
  {
    name: 'Restaurant',
    icon: `${ url }/images/icon/wine-bottle.png`
  },
  {
    name: 'Education',
    icon: `${ url }/images/icon/education.png`
  },
  {
    name: 'Books',
    icon: `${ url }/images/icon/books.png`
  },
  {
    name: 'Tuition Fee',
    icon: `${ url }/images/icon/money.png`
  },
  {
    name: 'Transportation',
    icon: `${ url }/images/icon/transportation.png`
  },
  {
    name: 'Petroleum',
    icon: `${ url }/images/icon/petro.png`
  },
  {
    name: 'Maintenance',
    icon: `${ url }/images/icon/maintenance.png`
  },
  {
    name: 'Taxi',
    icon: `${ url }/images/icon/taxi.png`
  },
  {
    name: 'Bus',
    icon: `${ url }/images/icon/bus.png`
  },
  {
    name: 'Parking fee',
    icon: `${ url }/images/icon/parking.png`
  },
  {
    name: 'Travel',
    icon: `${ url }/images/icon/travel.png`
  },
  {
    name: 'Family',
    icon: `${ url }/images/icon/house.png`
  },
  {
    name: 'Pet',
    icon: `${ url }/images/icon/pet.png`
  },
  {
    name: 'House',
    icon: `${ url }/images/icon/house.png`
  },
  {
    name: 'Bills & Utilties',
    icon: `${ url }/images/icon/money.png`
  },
  {
    name: 'Dunno',
    icon: `${ url }/images/icon/dunno.png`
  },
  {
    name: 'Internet',
    icon: `${ url }/images/icon/internet.png`
  },
  {
    name: 'TV',
    icon: `${ url }/images/icon/tv.png`
  },
  {
    name: 'Gas',
    icon: `${ url }/images/icon/gas.png`
  },
  {
    name: 'electricity',
    icon: `${ url }/images/icon/electricity.png`
  },
  {
    name: 'Water',
    icon: `${ url }/images/icon/water.png`
  },
  {
    name: 'Entertainment',
    icon: `${ url }/images/icon/entertainment.png`
  },
  {
    name: 'Games',
    icon: `${ url }/images/icon/game.png`
  },
  {
    name: 'Health & Fitness',
    icon: `${ url }/images/icon/heath-and-fitness.png`
  },
  {
    name: 'Drug',
    icon: `${ url }/images/icon/drug.png`
  },
  {
    name: 'Healthcare',
    icon: `${ url }/images/icon/health-care.png`
  },
  {
    name: 'Gift',
    icon: `${ url }/images/icon/gift.png`
  },
  {
    name: 'Birthday',
    icon: `${ url }/images/icon/birthday.png`
  },
  {
    name: 'Funeral',
    icon: `${ url }/images/icon/funeral.png`
  },
  {
    name: 'Investment',
    icon: `${ url }/images/icon/bill.png`
  },
  {
    name: 'Insurance',
    icon: `${ url }/images/icon/insurance.png`
  },
  {
    name: 'Friends',
    icon: `${ url }/images/icon/friends.png`
  },
  {
    name: 'Others',
    icon: `${ url }/images/icon/orthers.png`
  },
  {
    name: 'shopping',
    icon: `${ url }/images/icon/shopping.png`
  },
  {
    name: 'Clothes',
    icon: `${ url }/images/icon/clothes.png`
  },
  {
    name: 'Shoe',
    icon: `${ url }/images/icon/shoe.png`
  }
]