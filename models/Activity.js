const mongoose = require('mongoose')

const ActivitySchema = new mongoose.Schema({
  note: {
    type: String,
  },

  cost: {
    type: Number,
    required: true,
  },

  date: {
    type: Date,
    default: Date.now(),
  },

  category_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Category',
  },
  
  wallet_id: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'Wallet',
  },

  user_id: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'User',
  },

  __v: {
    type: Number,
    select: false,
  },

  created_at: Date,
  updated_at: Date,
})

ActivitySchema.pre('save', function(next) {
  const currentDate = Date.now()

  this.updated_at = currentDate

  if (!this.created_at)
    this.created_at = currentDate

  return next()
})

module.exports = mongoose.model('Activity', ActivitySchema)