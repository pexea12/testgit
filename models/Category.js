const mongoose = require('mongoose')

var CategorySchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },

  __v: {
    type: Number,
    select: false,
  },

  icon: {
    type: String
  },

  user_id: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'User',
  },

  created_at: Date,
  updated_at: Date,
})

CategorySchema.pre('save', function(next) {
  const currentDate = Date.now()

  this.updated_at = currentDate

  if (!this.created_at)
    this.created_at = currentDate

  return next()
})

module.exports = mongoose.model('Category', CategorySchema)