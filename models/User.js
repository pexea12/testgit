const bcrypt = require('bcryptjs')
const mongoose = require('mongoose')

const Wallet = require('models/Wallet')
const Activity = require('models/Activity')

const UserSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },

  email: {
    type: String,
    unique: true,
    required: true,
  },

  password: {
    type: String,
    required: true,
    select: false,
  },

  avatar: String,

  role: {
    type: String,
    default: 'member',
  },

  __v: {
    type: Number,
    select: false,
  },

  created_at: Date,
  updated_at: Date,
})

UserSchema.pre('save', function(next) {
  // Set time for created_at and updated_at
  const currentDate = Date.now()

  this.updated_at = currentDate

  if (!this.created_at)
      this.created_at = currentDate

  if (this.isModified('password') || this.isNew) {
    const user = this
    return bcrypt.genSalt(10, function(err, salt) {
      if (err) return next(err)
      bcrypt.hash(user.password, salt)
        .then((hash) => {
          user.password = hash
          next()
        }, err => next(err))
    })
  }

  next()
})

UserSchema.pre('remove', function(next) {
  Promise.all([
    Wallet.remove({ user_id: this._id }),
    Activity.remove({ user_id: this._id }),
  ])
    .then(() => next())
})

UserSchema.methods.comparePassword = function(password) {
  const user = this
  return new Promise(function(resolve, reject) {
    bcrypt.compare(password, user.password)
      .then(isMatched => resolve(isMatched))
  })
}

module.exports = mongoose.model('User', UserSchema);

