const mongoose = require('mongoose')

const Activity = require('models/Activity')

var WalletSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },

  currency: {
    type: String,
    default: 'USD',
  },

  inflow: {
    type: Number,
    default: 0,
  },

  outflow: {
    type: Number,
    default: 0,
  },
    
  user_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },

  __v: {
    type: Number,
    select: false,
  },

  icon: {
    type: String
  },

  created_at: Date,
  updated_at: Date,
})

WalletSchema.virtual('total').get(function() {
  return this.inflow - this.outflow;
})


WalletSchema.pre('save', function(next) {
  const currentDate = Date.now()

  this.updated_at = currentDate

  if (!this.created_at)
    this.created_at = currentDate

  return next()
})

WalletSchema.pre('remove', function(next) {
  Activity.remove({ wallet_id: this._id })
    .then(() => next())
})


module.exports = mongoose.model('Wallet', WalletSchema)